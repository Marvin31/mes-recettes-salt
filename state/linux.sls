adminitescia:
 user.present:
  - fullname: adminitescia
  - home: /home/admin
 
#htop:
# pkg.installed

pc-20.packages:
 pkg.installed:
   - pkgs:
     - htop
     - vim

AAAAB3NzaC1yc2EAAAABJQAAAQEAqS2Sf32/WKSBeENlcTtdLU193HFNUa0BmPjCEPbJD4VHNPIzDpeuBaciApDpL3BI1SBFQWf+krpmqEn5UKk9EZZR9hDcfg8EIYvGucIMnIqAtGmsMGoSMbba7aWZYWsvp9gQ3IlgjclqcB8xs9cvSR8i8TaMxhT/7eY1QYxgLCTcFZuJaFB5bLQitTTKmB/txXsaxuTQRW//WI7qD+YmPKIJL1Br9CtBixp5J+1kb7R/cuCxRLEtgGc8teFCCs3uG90GoAGiFlqlk6VHAZ9teMrHyhIlntwYzHIPnoRy/2OAqwtPYiUtKbiVY5UzxEovp25/bheji0nrzTkM2EqJFQ==:
 ssh_auth.present:
   - user: adminitescia
   - enc: ssh-rsa

serveursm:
 host.present:
   - ip:
     - 192.168.217.129
   - names:
     - saltmaster-20-1
